const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const voucherModel = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    code: {
        type: String,
        required: true,
        unique: true
    },
    discount: {
        type: Number,
        required: true
    },
    note: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model("voucher", voucherModel);
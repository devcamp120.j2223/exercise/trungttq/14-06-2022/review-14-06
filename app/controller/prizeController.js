const prizeModel = require("../models/prizeModel");

const mongoose = require("mongoose");

const createPrize  = (req, res) => {
    let body = req.body;
    if(!body.name){
        res.status(400).json({
            message: `name is require`
        })
    }else{
        let prize = {
            _id: mongoose.Types.ObjectId(),
            name: body.name
        }
        prizeModel.create(prize, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `create prize successfully`,
                    data: data
                })
            }
        })
    }
};

const getAllPrize  = (req, res) => {
    prizeModel.find((err, data) => {
        if(err){
            res.status(500).json({
                message: err.message
            })
        }else{
            res.status(201).json({
                message: `get all prize successfully`,
                data: data
            })
        }
    })
};

const getPrizeById  = (req, res) => {
    let id = req.params.prizeId;
    if(!mongoose.Types.ObjectId.isValid(id)){
        res.status(400).json({
            message: `prizeId is invalid`
        })
    }else{
        prizeModel.findById(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `get prize by id successfully`,
                    data: data
                })
            }
        })
    }
};

const updatePrizeById  = (req, res) => {
    let id = req.params.prizeId;
    let body = req.body;
    if(!mongoose.Types.ObjectId(id)){
        res.status(400).json({
            message: `prizeId is invalid`
        })
    }
    else if(!body.name){
        res.status(400).json({
            message: `name is require`
        })
    }else{
        let prize = {
            username: body.username,
            name: body.name
        }
        prizeModel.findByIdAndUpdate(id,prize, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `update prize successfully`,
                    data: data
                })
            }
        })
    }
};

const deletePrizeById  = (req, res) => {
    let id = req.params.prizeId;
    if(!mongoose.Types.ObjectId.isValid(id)){
        res.status(400).json({
            message: `prizeId is invalid`
        })
    }else{
        prizeModel.findByIdAndDelete(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(204).json({
                    message: `delete prize by id successfully`,
                    data: data
                })
            }
        })
    }
};

module.exports = {createPrize, getAllPrize, getPrizeById, updatePrizeById, deletePrizeById};